#ifndef INPUTEVENTMANAGER_H
#define INPUTEVENTMANAGER_H

#include <windows.h>
#include <WinUser.h>
#include <QSharedPointer>
#include <qtimer.h>

#include <QtMultimedia/qaudioinput.h>

#include "inputevent.h"

class InputEventListener;

class InputEventManager : public QIODevice
{
private:
    static QSharedPointer<InputEventListener> mListener;

    static HHOOK mKeyboardHook;
    static QTimer mMouseHandlerTimer;
    static QSharedPointer<QAudioInput> mAudioInput;
    static QSharedPointer<QIODevice> mAudioDevice;
    static QTimer mFftTimer;
    static QTimer mMusicTitleTimer;
    static HWND mMusicWindowHwnd;

public:
    static void Init(InputEvent::EType type);
    static void Deinit();

    static void SetListener(QSharedPointer<InputEventListener> listener);

private:
    static void MouseHandler();
    static LRESULT CALLBACK KeyboardProc(int code, WPARAM wParam, LPARAM lParam);
    qint64 readData(char *data, qint64 maxSize);
    qint64 writeData(const char *data, qint64 maxSize);
    static DWORD CALLBACK WasapiProc(void *buffer, DWORD length, void *user);
    static void FftHandler();
    static BOOL CALLBACK EnumWindowsProc(HWND hwnd, LPARAM lParam);
    static void MusicTitleHandler();

};

#endif // INPUTEVENTMANAGER_H
