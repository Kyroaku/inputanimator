import logging
import os
import subprocess
from shutil import copytree, copyfile

loggingFormat = "[%(levelname)s:%(name)s] %(message)s"
logging.basicConfig(level=logging.NOTSET, format=loggingFormat)
logger = logging.getLogger(__name__)


if __name__ == "__main__":
    windeployqt_path = r"C:\Qt\5.12.3\msvc2017\bin\windeployqt.exe"
    project_directory = r"D:\QtProjects\InputAnimator"
    binary_path = r"D:\QtProjects\InputAnimator\build-InputAnimator-Desktop_Qt_5_12_3_MSVC2017_32bit-Release\release\InputAnimator.exe"

    dependencies = [
        r"D:\QtProjects\InputAnimator\openssl-1.0.2p-i386-win32\libeay32.dll",
        r"D:\QtProjects\InputAnimator\openssl-1.0.2p-i386-win32\ssleay32.dll",
        r"D:\QtProjects\InputAnimator\bass24\bass.dll",
        r"D:\QtProjects\InputAnimator\basswasapi24\basswasapi.dll",
    ]

    local_animators = [
        # Bongo Cat
        r"D:\QtProjects\InputAnimator\Animators\BongoCat.qml",
        r"D:\QtProjects\InputAnimator\Animators\images\bongo cat",
        # Bongo Cat Mouse
        r"D:\QtProjects\InputAnimator\Animators\BongoCatMouse.qml",
        r"D:\QtProjects\InputAnimator\Animators\images\bongo mouse",
        # Bongo Cat Talk
        r"D:\QtProjects\InputAnimator\Animators\BongoCatTalk.qml",
        r"D:\QtProjects\InputAnimator\Animators\images\bongo cat talk",
        # Keyboard
        r"D:\QtProjects\InputAnimator\Animators\Keyboard.qml",
        r"D:\QtProjects\InputAnimator\Animators\images\keyboard",
        # Sound Spectrum
        r"D:\QtProjects\InputAnimator\Animators\SoundSpectrum.qml"
    ]

    logger.info("Configuring environment...")
    binary_directory = os.path.realpath(os.path.dirname(binary_path))
    deploy_directory = os.path.realpath(os.path.join(binary_directory, "..", "..", "deploy"))

    # Make sure deploy path exists
    if not os.path.exists(deploy_directory):
        os.mkdir(deploy_directory)

    binary_name = os.path.basename(binary_path)
    copyfile(binary_path, os.path.join(deploy_directory, binary_name))

    logger.debug("windeployqt path: " + windeployqt_path)
    logger.debug("QML directory: " + project_directory)
    logger.debug("Binary path: " + binary_path)
    logger.debug("Deploy directory: " + deploy_directory)

    logger.info("Deploying application...")
    process = subprocess.Popen([
        windeployqt_path,
        "--qmldir", project_directory,
        deploy_directory
    ], shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    stdout, stderr = process.communicate()
    if stderr:
        logger.warning(stderr)

    logger.info("Copying dependencies...")
    for dependency in dependencies:
        new_path = os.path.realpath(os.path.join(
            deploy_directory, os.path.basename(dependency)
        ))
        copyfile(dependency, new_path)

    for animator in local_animators:
        animator_local_path = animator.replace(project_directory, '')
        new_path = os.path.realpath(os.path.join(
            deploy_directory + animator_local_path
            ))
        if os.path.isdir(animator):
            copytree(animator, new_path)
        else:
            dstfolder = os.path.dirname(new_path)
            if not os.path.exists(dstfolder):
                os.makedirs(dstfolder)
            copyfile(animator, new_path)

    input("Deployed!")
