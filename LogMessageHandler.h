#ifndef LOGMESSAGEHANDLER_H
#define LOGMESSAGEHANDLER_H

#endif // LOGMESSAGEHANDLER_H

#include <QtGlobal>

void LogMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);
