#ifndef INPUTEVENTLISTENER_H
#define INPUTEVENTLISTENER_H

#include "inputevent.h"



class InputEventListener
{
public:
    virtual ~InputEventListener() {}
    virtual void OnInputEvent(InputEvent event) = 0;
};

#endif // INPUTEVENTLISTENER_H
