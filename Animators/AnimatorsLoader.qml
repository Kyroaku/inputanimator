import QtQuick 2.0

Item {
    property var animators: [
        "https://gitlab.com/Kyroaku/inputanimator/-/raw/release_v3/Animators/HighNoonJhin.qml",
        "https://gitlab.com/Kyroaku/inputanimator/-/raw/release_v3/Animators/Teemo.qml",
        "https://gitlab.com/Kyroaku/inputanimator/-/raw/release_v3/Animators/MusicSpectrum1.qml",
        "https://gitlab.com/Kyroaku/inputanimator/-/raw/release_v3/Animators/MusicSpectrum2.qml",
    ]
}
