import QtQuick 2.0
import inputevent 1.0
import animator 1.0

Item {
    width: closed_mouth.width
    height: closed_mouth.height

    Image {
        id: open_mouth
        visible: false
        source: "images/pop cat/open_mouth.png"
    }

    Image {
        id: closed_mouth
        visible: true
        source: "images/pop cat/closed_mouth.png"
    }

    Timer {
        id: close_mouth_timer
        interval: 100
        repeat: false

        onTriggered: {
                open_mouth.visible = false;
                closed_mouth.visible = true;
        }
    }

    Animator {
        eventType: InputEvent.TypeVoice

        onActivate: {
            if(inputVoice > 1400)
            {
                open_mouth.visible = true;
                closed_mouth.visible = false;
                close_mouth_timer.restart()
            }
        }
    }
}


