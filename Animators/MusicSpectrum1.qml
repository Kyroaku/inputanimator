import QtQuick 2.0
import inputevent 1.0
import animator 1.0
import QtQuick.Layouts 1.3

Item {
    width: 600
    height: 200
    id: root
    property var bins : []

    function createImage(x, y, w, h) {
        return Qt.createQmlObject(
                    "import QtQuick 2.0
                        Rectangle {
                        radius: 4
                        x: " + x + "
                        y: " + y + "
                        z: 1
                        width: " + w + "
                        height: " + h + "
                    }",
                    root
                    )
    }

    Component.onCompleted: {
        bins.length = 64
        for(var i = 0; i < 64; i++) {
            bins[i] = createImage(width/64.0*i, height - i/64.0*height, width/64.0, height)
            bins[i].color = Qt.hsva(i/64 * 0.5 + 0.5,1,1,1)
        }
    }

    Animator {
        eventType: InputEvent.TypeSound

        onActivate: {
            var k = 0.2
            for(var i = 0; i < inputSound.length; i++)
            {
                bins[i].y = k*(height - inputSound[i]*height) + (1.0-k)*bins[i].y
            }
        }
    }

    Text {
        id: musicTitleText
        y: 32
        z: 2
        color: "#ffffff"
        text: qsTr("Open browser with YouTube card")
        anchors.bottom: parent.bottom
        styleColor: "#ffffff"
        font.bold: true
        font.family: "Courier"
        font.pixelSize: 15

        onWidthChanged: {
            textAnimation.to = -width
            textAnimation.duration = width*12000/500
            textAnimation.restart()
        }

        NumberAnimation on x {
            id: textAnimation
            running: true
            duration: 12000
            loops: Animation.Infinite
            from: 700; to: -musicTitleText.width
        }

        Animator {
            eventType: InputEvent.TypeMusicTitle

            onActivate: {
                musicTitleText.text = inputMusicTitle
            }
        }
    }
}



/*##^##
Designer {
    D{i:2;anchors_x:102;anchors_y:32}
}
##^##*/
