import QtQuick 2.0
import inputevent 1.0
import animator 1.0

Item {
    width: background.width
    height: background.height

    Image {
        id: background
        source: "images/teemo/background.png"
    }

    Image {
        id: mouse_off
        visible: true
        source: {
            if(Math.random() > 0.5)
                "images/teemo/mouse_off.png"
            else
                "images/teemo/fuck_off.png"
        }
    }
    Image {
        id: mouse_on
        visible: false
        source: "images/teemo/mouse_on.png"
    }

    Image {
        id: keyboard_off
        visible: true
        source: "images/teemo/keyboard_off.png"
    }
    Image {
        id: keyboard_q
        visible: false
        source: "images/teemo/q.png"
    }
    Image {
        id: keyboard_w
        visible: false
        source: "images/teemo/w.png"
    }
    Image {
        id: keyboard_r
        visible: false
        source: "images/teemo/r.png"
    }

    Animator {
        eventType: InputEvent.TypeKeyboardKey
        eventKey: 'Q'.charCodeAt(0)
        eventKeyState: InputEvent.StatePressed

        onActivate: {
            keyboard_q.visible = true
            keyboard_w.visible = false
            keyboard_r.visible = false
            keyboard_off.visible = false
        }
    }

    Animator {
        eventType: InputEvent.TypeKeyboardKey
        eventKey: 'W'.charCodeAt(0)
        eventKeyState: InputEvent.StatePressed

        onActivate: {
            keyboard_q.visible = false
            keyboard_w.visible = true
            keyboard_r.visible = false
            keyboard_off.visible = false
        }
    }

    Animator {
        eventType: InputEvent.TypeKeyboardKey
        eventKey: 'R'.charCodeAt(0)
        eventKeyState: InputEvent.StatePressed

        onActivate: {
            keyboard_q.visible = false
            keyboard_w.visible = false
            keyboard_r.visible = true
            keyboard_off.visible = false
        }
    }

    Animator {
        eventType: InputEvent.TypeKeyboardKey
        eventKey: InputEvent.KeyAny
        eventKeyState: InputEvent.StateReleased

        onActivate: {
            keyboard_q.visible = false
            keyboard_w.visible = false
            keyboard_r.visible = false
            keyboard_off.visible = true
        }
    }

    Animator {
        eventType: InputEvent.TypeMouseButton
        eventMouseButton: InputEvent.ButtonAny
        eventKeyState: InputEvent.StatePressed

        onActivate: {
            mouse_on.visible = true
            mouse_off.visible = false
        }
    }

    Animator {
        eventType: InputEvent.TypeMouseButton
        eventMouseButton: InputEvent.ButtonAny
        eventKeyState: InputEvent.StateReleased

        onActivate: {
            mouse_on.visible = false
            mouse_off.visible = true
        }
    }
}


