import QtQuick 2.0
import inputevent 1.0
import animator 1.0

Item {
    width: keyboard_inactive.width
    height: keyboard_inactive.height

    Image {
        id: barckground
        source: "images/highnoon_jhin/background.png"
    }
    Image {
        id: keyboard_active
        visible: false
        source: "images/highnoon_jhin/keyboard_on.png"
    }
    Image {
        id: keyboard_inactive
        visible: true
        source: "images/highnoon_jhin/keyboard_off.png"
    }

    Image {
        id: mouse_active
        visible: false
        source: "images/highnoon_jhin/mouse_on.png"
    }
    Image {
        id: mouse_inactive
        visible: true
        source: "images/highnoon_jhin/mouse_off.png"
    }

    Image {
        id: speak
        visible: false
        source: "images/highnoon_jhin/speak.png"
    }

    Animator {
        eventType: InputEvent.TypeKeyboardKey
        eventKey: InputEvent.KeyAny
        eventKeyState: InputEvent.StatePressed

        onActivate: {
            keyboard_active.visible = true
            keyboard_inactive.visible = false
        }
    }

    Animator {
        eventType: InputEvent.TypeKeyboardKey
        eventKey: InputEvent.KeyAny
        eventKeyState: InputEvent.StateReleased

        onActivate: {
            keyboard_active.visible = false
            keyboard_inactive.visible = true
        }
    }

    Animator {
        eventType: InputEvent.TypeMouseButton
        eventMouseButton: InputEvent.ButtonAny
        eventKeyState: InputEvent.StatePressed

        onActivate: {
            mouse_active.visible = true
            mouse_inactive.visible = false
        }
    }

    Animator {
        eventType: InputEvent.TypeMouseButton
        eventMouseButton: InputEvent.ButtonAny
        eventKeyState: InputEvent.StateReleased

        onActivate: {
            mouse_active.visible = false
            mouse_inactive.visible = true
        }
    }

    Animator {
        eventType: InputEvent.TypeVoice

        onActivate: {
            if(inputVoice > 1900)
                speak.visible = true;
            else
                speak.visible = false
        }
    }
}


