import QtQuick 2.0
import inputevent 1.0
import animator 1.0
import QtQuick.Layouts 1.3

/* Documentation

  Animator properties:

    eventType           - event type that triggers animator
    eventKeyState       - state of the key that triggers animator
    eventKey            - (if keyboard key event type) keyboard key that triggers animator
    eventMouseButton    - (if mouse button event type) mouse button that triggers animator

    'onActivate' function parameters:
        Those variables are available inside onActivate function and holds information about
        that what excacly triggered the animator.

        inputType       - type of the event that triggered animator
        inputKeyState   - state of the keyboard key/mouse button that triggered AnimatorsManager
        inputKey        - keyboard key ASCII code that triggered animator
        inputButton     - mouse button that triggered animator

  Possible values:

    eventType/inputType:
        - InputEvent.TypeKeyboardKey
        - InputEvent.TypeMouseButton
        - InputEvent.TypeMouseMove

    eventKeyState/inputKeyState:
        - InputEvent.StatePressed
        - InputEvent.StateReleased

    eventKey/inputKey:
        - key ASCII code (for example: 65 - 'A')
        - InputEvent.KeyAny

    eventMouseButton/inputButton:
        - InputEvent.ButtonLeft
        - InputEvent.ButtonRight
 */

Item {
    width: gridLayout.width
    height: gridLayout.height

    // Register event that will trigger when user presses any keyboard key.
    Animator {
        eventType: InputEvent.TypeKeyboardKey   // On keyboard key event.
        eventKeyState: InputEvent.StatePressed  // On key pressed.
        eventKey: InputEvent.KeyAny             // On any key.

        onActivate: {
            // Function invoked each time event is triggered.
            colorRect.color = "green"
        }
    }

    // Register event that will trigger when user releases any keyboard key.
    Animator {
        eventType: InputEvent.TypeKeyboardKey   // On keyboard key event.
        eventKeyState: InputEvent.StateReleased // On key released.
        eventKey: InputEvent.KeyAny             // On any key.

        onActivate: {
            // Function invoked each time event is triggered.
            colorRect.color = "red"
        }
    }

    // Register event that will trigger when user presses/releases any mouse button.
    Animator {
        eventType: InputEvent.TypeMouseButton   // On mouse button event.
        eventKeyState: InputEvent.StateAny      // On button pressed/released (any).
        eventMouseButton: InputEvent.ButtonAny  // On any button.

        onActivate: {
            // Function invoked each time event is triggered.

            // We can check key state by reading 'inputKeyState' variable.
            if(inputKeyState === InputEvent.StatePressed)
                buttonText.text = "Mouse\npressed"
            if(inputKeyState === InputEvent.StateReleased)
                buttonText.text = "Mouse\nreleased"
        }
    }

    GridLayout {
        id: gridLayout
        columns: 2

        Text {
            id: mousePosText
            text: qsTr("0x0")
            lineHeight: 1
            Layout.fillHeight: false
            Layout.fillWidth: false
            font.bold: true
            font.family: "Tahoma"
            font.pixelSize: 27

            // Events can be registered also inside other components (same result).
            // Register event that will trigger when user moves the mouse.
            Animator {
                eventType: InputEvent.TypeMouseMove // On mouse move event.

                onActivate: {
                    // Function invoked each time event is triggered.

                    // We can read mouse position by using 'inputMouse.x' and 'inputMouse.y' variables.
                    mousePosText.text = "x: " + inputMouse.x.toFixed(2) + "\ny: " + inputMouse.y.toFixed(2)
                }
            }
        }

        Rectangle {
            id: colorRect
            width: 100
            height: 100
            color: "red"
            border.width: 1

            Text {
                id: element
                text: qsTr("KeyState")
                anchors.fill: parent
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.family: "Courier"
                font.pixelSize: 15
            }
        }

        Text {
            id: buttonText
            text: qsTr("Mouse\nreleased")
            lineHeight: 1
            Layout.fillHeight: false
            Layout.fillWidth: false
            font.bold: true
            font.family: "Tahoma"
            font.pixelSize: 27
        }

        Text {
            id: speakText
            text: qsTr("Silence")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            Layout.fillHeight: true
            Layout.fillWidth: true
            font.pixelSize: 12

            Animator {
                eventType: InputEvent.TypeVoice

                onActivate: {
                    if(inputVoice > 1700)
                        speakText.text = "speaking"
                    else
                        speakText.text = "silence"
                }
            }
        }
    }
}



/*##^##
Designer {
    D{i:2;anchors_height:100;anchors_width:100}D{i:8;anchors_height:13;anchors_width:52}
}
##^##*/
