import QtQuick 2.0
import inputevent 1.0
import animator 1.0

Item {
    width: keyboard_active.width
    height: keyboard_active.height

//    Text {
//        enabled: true

//        id: mouseText
//        anchors.fill: parent
//        text: "0x0"

//        MouseArea {
//            anchors.fill: parent
//            propagateComposedEvents: true
//            onPositionChanged: {
//                mouseText.text = mouseX + "x" + mouseY
//            }
//        }
//    }

    Image {
        id: background
        source: "images/bongo mouse/background.png"
    }

    Image {
        id: keyboard_active
        visible: false
        source: "images/bongo mouse/keyboard_on.png"
    }
    Image {
        id: keyboard_inactive
        visible: true
        source: "images/bongo mouse/keyboard_off.png"
    }

    Image {
        id: mouseImage
        scale: 0.6
        x: 72 - width/2; y: 155-height/2
        visible: true
        source: "images/bongo mouse/mouse.png"
    }

    Canvas {
        anchors.fill: parent
        contextType: "2d"

        id: mouseCanvas

        Path {
            id: myPath
            startX: 76; startY: 131
            PathCurve {
                x: 72; y: 140
            }
            PathCurve {
                id: mousePos
                x: 72; y: 155
            }
            PathCurve { x: 104; y: 139 }
        }

        onPaint: {
            if(context) {
                context.reset();
                context.path = myPath;
                context.lineWidth = 2
                context.strokeStyle = 'black'
                context.fillStyle = "white";
                context.fill()
                context.stroke();
            }
        }
    }

    Animator {
        eventType: InputEvent.TypeKeyboardKey
        eventKey: InputEvent.KeyAny
        eventKeyState: InputEvent.StatePressed

        onActivate: {
            keyboard_active.visible = true
            keyboard_inactive.visible = false
        }
    }

    Animator {
        eventType: InputEvent.TypeKeyboardKey
        eventKey: InputEvent.KeyAny
        eventKeyState: InputEvent.StateReleased

        onActivate: {
            keyboard_active.visible = false
            keyboard_inactive.visible = true
        }
    }

    Animator {
        eventType: InputEvent.TypeMouseMove

        onActivate: {
            mousePos.x =
                    (1.0 - inputMouse.x)*inputMouse.y*92 +
                    (1.0 - inputMouse.x)*(1.0-inputMouse.y)*86 +
                    inputMouse.x*inputMouse.y*80 +
                    inputMouse.x*(1.0-inputMouse.y)*71
            mousePos.y =
                    (1.0 - inputMouse.x)*inputMouse.y*153 +
                    (1.0 - inputMouse.x)*(1.0-inputMouse.y)*167 +
                    inputMouse.x*inputMouse.y*147 +
                    inputMouse.x*(1.0-inputMouse.y)*160

            mouseImage.x = mousePos.x - (mouseImage.width / 2) - 5
            mouseImage.y = mousePos.y - (mouseImage.height / 2) + 5

            mouseCanvas.requestPaint()
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
