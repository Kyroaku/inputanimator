import QtQuick 2.0
import inputevent 1.0
import animator 1.0

Item {
    id: root
    width: keyboard_layout.width * 0.5
    height: keyboard_layout.height * 0.5

    transform: Scale { xScale: 0.5; yScale: 0.5 }

    property var keymap

    function createImage(path) {
        return Qt.createQmlObject(
                    "import QtQuick 2.0
                        Image {
                        visible: false
                        width: keyboard_layout.width
                        height: keyboard_layout.height
                        source: '" + path + "'" +
                    "}",
                    root
                    )
    }

    Component.onCompleted: {
        keymap = new Map()
        keymap.set(38, createImage("images/keyboard/w.png"))
        keymap.set(40, createImage("images/keyboard/s.png"))
        keymap.set(37, createImage("images/keyboard/a.png"))
        keymap.set(39, createImage("images/keyboard/d.png"))
        keymap.set('Q'.charCodeAt(0), createImage("images/keyboard/q.png"))
        keymap.set('W'.charCodeAt(0), createImage("images/keyboard/w.png"))
        keymap.set('E'.charCodeAt(0), createImage("images/keyboard/e.png"))
        keymap.set('R'.charCodeAt(0), createImage("images/keyboard/r.png"))
        keymap.set('T'.charCodeAt(0), createImage("images/keyboard/t.png"))
        keymap.set('Y'.charCodeAt(0), createImage("images/keyboard/y.png"))
        keymap.set('U'.charCodeAt(0), createImage("images/keyboard/u.png"))
        keymap.set('I'.charCodeAt(0), createImage("images/keyboard/i.png"))
        keymap.set('O'.charCodeAt(0), createImage("images/keyboard/o.png"))
        keymap.set('P'.charCodeAt(0), createImage("images/keyboard/p.png"))
        keymap.set('A'.charCodeAt(0), createImage("images/keyboard/a.png"))
        keymap.set('S'.charCodeAt(0), createImage("images/keyboard/s.png"))
        keymap.set('D'.charCodeAt(0), createImage("images/keyboard/d.png"))
        keymap.set('F'.charCodeAt(0), createImage("images/keyboard/f.png"))
        keymap.set('G'.charCodeAt(0), createImage("images/keyboard/g.png"))
        keymap.set('H'.charCodeAt(0), createImage("images/keyboard/h.png"))
        keymap.set('J'.charCodeAt(0), createImage("images/keyboard/j.png"))
        keymap.set('K'.charCodeAt(0), createImage("images/keyboard/k.png"))
        keymap.set('L'.charCodeAt(0), createImage("images/keyboard/l.png"))
        keymap.set('Z'.charCodeAt(0), createImage("images/keyboard/z.png"))
        keymap.set('X'.charCodeAt(0), createImage("images/keyboard/x.png"))
        keymap.set('C'.charCodeAt(0), createImage("images/keyboard/c.png"))
        keymap.set('V'.charCodeAt(0), createImage("images/keyboard/v.png"))
        keymap.set('B'.charCodeAt(0), createImage("images/keyboard/b.png"))
        keymap.set('N'.charCodeAt(0), createImage("images/keyboard/n.png"))
        keymap.set('M'.charCodeAt(0), createImage("images/keyboard/m.png"))
        keymap.set('1'.charCodeAt(0), createImage("images/keyboard/1.png"))
        keymap.set('2'.charCodeAt(0), createImage("images/keyboard/2.png"))
        keymap.set('3'.charCodeAt(0), createImage("images/keyboard/3.png"))
        keymap.set('4'.charCodeAt(0), createImage("images/keyboard/4.png"))
        keymap.set('5'.charCodeAt(0), createImage("images/keyboard/5.png"))
        keymap.set('6'.charCodeAt(0), createImage("images/keyboard/6.png"))
        keymap.set('7'.charCodeAt(0), createImage("images/keyboard/7.png"))
        keymap.set('8'.charCodeAt(0), createImage("images/keyboard/8.png"))
        keymap.set('9'.charCodeAt(0), createImage("images/keyboard/9.png"))
        keymap.set('0'.charCodeAt(0), createImage("images/keyboard/0.png"))
        keymap.set(164, createImage("images/keyboard/lalt.png"))
        keymap.set(162, createImage("images/keyboard/lctrl.png"))
        keymap.set(160, createImage("images/keyboard/lshift.png"))
        keymap.set(9, createImage("images/keyboard/tab.png"))
        keymap.set(20, createImage("images/keyboard/caps.png"))
        keymap.set(13, createImage("images/keyboard/enter.png"))
        keymap.set(8, createImage("images/keyboard/backspace.png"))
        keymap.set(32, createImage("images/keyboard/space.png"))
    }

    Image {
        id: keyboard_layout
        visible: true
        source: "images/keyboard/layout.png"
    }

    Animator {
        eventType: InputEvent.TypeKeyboardKey
        eventKey: InputEvent.KeyAny
        eventKeyState: InputEvent.StateAny

        onActivate: {
            if(keymap.has(inputKey))
                keymap.get(inputKey).visible = inputKeyState == InputEvent.StatePressed
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
