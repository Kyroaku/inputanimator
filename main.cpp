#include "animatorsmanager.h"

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlComponent>
#include <QTimer>
#include <qquickitem.h>
#include <QQmlContext>

#include <QDir>
#include <QQuickView>
#include <inputeventmanager.h>
#include <LogMessageHandler.h>
#include <qdebug.h>


int main(int argc, char *argv[])
{
#ifdef QT_NO_DEBUG
    qInstallMessageHandler(LogMessageHandler);
#endif

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

#ifdef QT_DEBUG
    QDir::setCurrent("D:/QtProjects/InputAnimator");
    qDebug() << "[main] Working directory set: " << QDir::currentPath();
#endif

    QSharedPointer<AnimatorsManager> animatorsManager(new AnimatorsManager);

    engine.rootContext()->setContextProperty("AnimatorsManager", animatorsManager.get());

    qmlRegisterType<Animator>("animator", 1, 0, "Animator");
    qmlRegisterType<InputEvent>("inputevent", 1, 0, "InputEvent");
    qRegisterMetaType<InputEvent::EType>();
    qRegisterMetaType<InputEvent::EKeyState>();
    qRegisterMetaType<InputEvent::EMouseButton>();

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl) {
            QCoreApplication::exit(-1);
        }
    }, Qt::QueuedConnection);
    engine.load(url);

    InputEventManager::SetListener(animatorsManager);

    return app.exec();
}
