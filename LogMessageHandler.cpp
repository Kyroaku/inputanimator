#include "LogMessageHandler.h"

#include <QFile>
#include <QDir>
#include <QDateTime>
#include <qdebug.h>

void LogMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    Q_UNUSED(context)

    static QFile file;
    if(!file.isOpen())
    {
        QString logsDirName = "logs";

        QDir root = QDir::current();

        // Make sure directory exists.
        if(!root.exists(logsDirName))
            root.mkdir(logsDirName);
        root.cd(logsDirName);

        auto date = QDateTime::currentDateTime().toString("dd-MM-yyyy hh-mm-ss");
        auto filename = root.filePath(date + ".txt");
        qDebug() << "Logs file: " << filename;

        file.setFileName(filename);
        if(!file.open(QFile::ReadWrite))
        {
            qDebug() << "Couldn't open log file.";
            abort();
        }
    }

    QString message;

    switch(type)
    {
    case QtDebugMsg:
        message = "[Debug] " + msg;
        break;

    default:
        message = "[Log] " + msg;
        break;
    }

    file.write(message.toLocal8Bit() + "\n");
}
