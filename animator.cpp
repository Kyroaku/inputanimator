#include "animator.h"

#include <qguiapplication.h>
#include <QScreen>

Animator::Animator()
{
    setObjectName("InputAnimator");
}

Animator::~Animator()
{

}

void Animator::OnActivate(InputEvent event)
{
    QRect rec = QGuiApplication::primaryScreen()->geometry();
    QPointF normalizedMouse(
                fmax(0.0, fmin(1.0, qreal(event.mMouse.x()) / rec.width())),
                fmax(0.0, fmin(1.0, qreal(event.mMouse.y()) / rec.height()))
                );
    emit activate(
                event.mType,
                event.mKeyState,
                event.mKey,
                event.mMouseButton,
                normalizedMouse,
                event.mVoice,
                event.mSound,
                event.mMusicTitle
                );
}

InputEvent::EType Animator::eventType()
{
    return mEvent.mType;
}

InputEvent::EKeyState Animator::eventKeyState()
{
    return mEvent.mKeyState;
}

InputEvent::KeyType Animator::eventKey()
{
    return mEvent.mKey;
}

InputEvent::EMouseButton Animator::eventMouseButton()
{
    return mEvent.mMouseButton;
}

QPoint Animator::eventMouse()
{
    return mEvent.mMouse;
}

void Animator::setEventType(InputEvent::EType type)
{
    mEvent.mType = type;
}

void Animator::setEventKeyState(InputEvent::EKeyState state)
{
    mEvent.mKeyState = state;
}

void Animator::setEventKey(InputEvent::KeyType key)
{
    mEvent.mKey = key;
}

void Animator::setEventMouseButton(InputEvent::EMouseButton button)
{
    mEvent.mMouseButton = button;
}

void Animator::setEventMouse(QPoint mouse)
{
    mEvent.mMouse = mouse;
}
