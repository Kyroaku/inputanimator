#ifndef INPUTEVENT_H
#define INPUTEVENT_H

#include <QPoint>
#include <qobject.h>
#include <qtypeinfo.h>
#include <qvector.h>

class InputEvent : public QObject
{
    Q_OBJECT

public:
    typedef int KeyType;
    typedef int VoiceType;

    enum class EType
    {
        TypeAny,
        TypeKeyboardKey,
        TypeMouseButton,
        TypeMouseMove,
        TypeVoice,
        TypeSound,
        TypeMusicTitle
    };
    Q_ENUM(EType)

    enum class EKeyState
    {
        StateAny,
        StateReleased,
        StatePressed
    };
    Q_ENUM(EKeyState)

    enum EKey
    {
        KeyAny = 0
    };
    Q_ENUM(EKey)

    enum class EMouseButton
    {
        ButtonAny,
        ButtonLeft,
        ButtonRight
    };
    Q_ENUM(EMouseButton)

public:
    InputEvent() {}
    InputEvent(const InputEvent &c)
        : mType(c.mType)
        , mKeyState(c.mKeyState)
        , mKey(c.mKey)
        , mMouse(c.mMouse)
        , mMouseButton(c.mMouseButton)
        , mVoice(c.mVoice)
        , mSound(c.mSound)
        , mMusicTitle(c.mMusicTitle)
    {
    }

public:
    EType mType;
    EKeyState mKeyState;
    KeyType mKey = 0;
    QPoint mMouse;
    EMouseButton mMouseButton;
    VoiceType mVoice;
    QVector<qreal> mSound;
    QString mMusicTitle;
};

#endif // INPUTEVENT_H
