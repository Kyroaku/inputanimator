#include "inputeventmanager.h"
#include "inputeventlistener.h"
#include "inputevent.h"

#include "bass.h"
#include "basswasapi.h"

#include <QElapsedTimer>

#include <Windows.h>

#include <qdebug.h>

HHOOK InputEventManager::mKeyboardHook = NULL;
QSharedPointer<InputEventListener> InputEventManager::mListener = nullptr;
QTimer InputEventManager::mMouseHandlerTimer;
QSharedPointer<QAudioInput> InputEventManager::mAudioInput;
QSharedPointer<QIODevice> InputEventManager::mAudioDevice;
QTimer InputEventManager::mFftTimer;
QTimer InputEventManager::mMusicTitleTimer;
HWND InputEventManager::mMusicWindowHwnd = NULL;

void InputEventManager::Init(InputEvent::EType type)
{
    qDebug() << "Event registered: " << type;
    switch(type) {
    case InputEvent::EType::TypeKeyboardKey: {
        /* Init keyboard event listener. */
        if(mKeyboardHook)
            break;

        mKeyboardHook = SetWindowsHookExA(WH_KEYBOARD_LL, &KeyboardProc, nullptr, 0);
        break;
    }

    case InputEvent::EType::TypeMouseButton:
    case InputEvent::EType::TypeMouseMove: {
        /* Init mouse event listener. */
        if(mMouseHandlerTimer.isActive())
            return;

        mMouseHandlerTimer.setInterval(1000 / 60);
        QObject::connect(&mMouseHandlerTimer, &QTimer::timeout, &InputEventManager::MouseHandler);
        mMouseHandlerTimer.start();
        break;
    }

    case InputEvent::EType::TypeVoice: {
        /* Init voice event listener. */
        if(mAudioInput)
            break;

        const QAudioDeviceInfo inputDeviceInfo = QAudioDeviceInfo::defaultInputDevice();
        if (inputDeviceInfo.isNull()) {
            qDebug() << "No input device found!";
        } else {
            qDebug() << "Input device: " << inputDeviceInfo.deviceName();
        }

        QAudioFormat formatAudio;
        formatAudio.setSampleRate(44100);
        formatAudio.setChannelCount(1);
        formatAudio.setSampleSize(16);
        formatAudio.setCodec("audio/pcm");
        formatAudio.setByteOrder(QAudioFormat::LittleEndian);
        formatAudio.setSampleType(QAudioFormat::UnSignedInt);

        mAudioInput = QSharedPointer<QAudioInput>(new QAudioInput(inputDeviceInfo, formatAudio));

        mAudioDevice = QSharedPointer<QIODevice>(new InputEventManager);
        mAudioDevice->open(QIODevice::WriteOnly);
        mAudioInput->setBufferSize(44100/8);
        mAudioInput->start(mAudioDevice.get());
        break;
    }

    case InputEvent::EType::TypeSound: {
        /* Init sound FFT event listener. */
        if(mFftTimer.isActive())
            break;

        if(!BASS_Init(0, 44100, 0, 0, 0))
            qDebug() << "[Bass] Init failed with code: " << BASS_ErrorGetCode();

        if(!BASS_WASAPI_Init(-3, 0, 0, BASS_WASAPI_BUFFER, 0, 0, WasapiProc, NULL))
            qDebug() << "[Bass] Init failed with code: " << BASS_ErrorGetCode();

        if(!BASS_WASAPI_Start())
            qDebug() << "[Bass] Start failed with code: " << BASS_ErrorGetCode();

        mFftTimer.setInterval(16);
        QObject::connect(&mFftTimer, &QTimer::timeout, &InputEventManager::FftHandler);
        mFftTimer.start();
        break;
    }

    case InputEvent::EType::TypeMusicTitle: {
        /* Init music title event listener. */
        if(mMusicTitleTimer.isActive())
            break;

        mMusicTitleTimer.setInterval(3000);
        QObject::connect(&mMusicTitleTimer, &QTimer::timeout, &InputEventManager::MusicTitleHandler);
        mMusicTitleTimer.start();
        break;
    }
    }
}

void InputEventManager::Deinit()
{
    /* Deinit keyboard */
    if(mKeyboardHook) {
        UnhookWindowsHookEx(mKeyboardHook);
        mKeyboardHook = NULL;
    }

    /* Deinit mouse */
    mMouseHandlerTimer.stop();
    mMouseHandlerTimer.disconnect();

    /* Deinit voice */
    if(mAudioInput) {
        mAudioInput->stop();
        mAudioInput = nullptr;
    }
    if(mAudioDevice) {
        mAudioDevice->close();
        mAudioDevice = nullptr;
    }

    /* Deinit fft */
    mFftTimer.stop();
    mFftTimer.disconnect();
    BASS_WASAPI_Stop(true);
    BASS_WASAPI_Free();
    BASS_Free();

    /* Deinit music title */
    mMusicTitleTimer.stop();
    mMusicTitleTimer.disconnect();
}

void InputEventManager::SetListener(QSharedPointer<InputEventListener> listener)
{
    mListener = listener;
}

void InputEventManager::MouseHandler()
{
    static bool mouseButtons[3] = {false};
    InputEvent event;

    POINT mouse;
    GetCursorPos(&mouse);
    event.mType = InputEvent::EType::TypeMouseMove;
    event.mMouse = QPoint(mouse.x, mouse.y);
    if(mListener)
        mListener.get()->OnInputEvent(event);

    event.mType = InputEvent::EType::TypeMouseButton;
    event.mMouseButton = InputEvent::EMouseButton::ButtonRight;
    event.mKeyState = InputEvent::EKeyState::StateAny;
    if ((GetKeyState(VK_RBUTTON) & 0x80) != 0)
    {
        if(!mouseButtons[0])
            event.mKeyState = InputEvent::EKeyState::StatePressed;
        mouseButtons[0] = true;
    }
    else
    {
        if(mouseButtons[0])
            event.mKeyState = InputEvent::EKeyState::StateReleased;
        mouseButtons[0] = false;
    }
    if(mListener && event.mKeyState != InputEvent::EKeyState::StateAny)
        mListener.get()->OnInputEvent(event);

    event.mType = InputEvent::EType::TypeMouseButton;
    event.mMouseButton = InputEvent::EMouseButton::ButtonLeft;
    event.mKeyState = InputEvent::EKeyState::StateAny;
    if ((GetKeyState(VK_LBUTTON) & 0x80) != 0)
    {
        if(!mouseButtons[1])
            event.mKeyState = InputEvent::EKeyState::StatePressed;
        mouseButtons[1] = true;
    }
    else
    {
        if(mouseButtons[1])
            event.mKeyState = InputEvent::EKeyState::StateReleased;
        mouseButtons[1] = false;
    }
    if(mListener && event.mKeyState != InputEvent::EKeyState::StateAny)
        mListener.get()->OnInputEvent(event);
}

LRESULT CALLBACK InputEventManager::KeyboardProc(int code, WPARAM wParam, LPARAM lParam)
{
    /* Documentation says that function should return immediately when code is less than 0. */
    if(code < 0)
        return CallNextHookEx(nullptr, code, wParam, lParam);

    KBDLLHOOKSTRUCT *kb = reinterpret_cast<KBDLLHOOKSTRUCT*>(lParam);

    InputEvent event;

    switch(wParam)
    {
    case WM_KEYDOWN:
    case WM_SYSKEYDOWN:
        event.mType = InputEvent::EType::TypeKeyboardKey;
        event.mKeyState = InputEvent::EKeyState::StatePressed;
        event.mKey = kb->vkCode;
        break;

    case WM_KEYUP:
    case WM_SYSKEYUP:
        event.mType = InputEvent::EType::TypeKeyboardKey;
        event.mKeyState = InputEvent::EKeyState::StateReleased;
        event.mKey = kb->vkCode;
        break;

        //    case WM_LBUTTONDOWN:
        //        event.mType = InputEvent::EType::MouseButton;
        //        event.mKeyState = InputEvent::EKeyState::Pressed;
        //        event.mMouseButton = InputEvent::EMouseButton::Left;
        //        break;

        //    case WM_LBUTTONUP:
        //        event.mType = InputEvent::EType::MouseButton;
        //        event.mKeyState = InputEvent::EKeyState::Released;
        //        event.mMouseButton = InputEvent::EMouseButton::Left;
        //        break;

        //    case WM_RBUTTONDOWN:
        //        event.mType = InputEvent::EType::MouseButton;
        //        event.mKeyState = InputEvent::EKeyState::Pressed;
        //        event.mMouseButton = InputEvent::EMouseButton::Right;
        //        break;

        //    case WM_RBUTTONUP:
        //        event.mType = InputEvent::EType::MouseButton;
        //        event.mKeyState = InputEvent::EKeyState::Released;
        //        event.mMouseButton = InputEvent::EMouseButton::Right;
        //        break;

        //    case WM_MOUSEMOVE:
        //        event.mType = InputEvent::EType::MouseMove;
        //        event.mMouse = QPoint(mouse->pt.x, mouse->pt.y);
        //        break;

    default:
        return CallNextHookEx(nullptr, code, wParam, lParam);
    }

    if(mListener)
        mListener.get()->OnInputEvent(event);

    return CallNextHookEx(nullptr, code, wParam, lParam);
}

qint64 InputEventManager::readData(char *data, qint64 maxSize)
{
    Q_UNUSED(data)
    Q_UNUSED(maxSize)
    return -1;
}

qint64 InputEventManager::writeData(const char *data, qint64 maxSize)
{
    static QElapsedTimer timer;

    double average = 0.0;
    int max = -(1<<15);
    for(qint64 i = 0; i < maxSize; i+=2, data+=2) {
        qint16 value;
        memcpy(&value, data, 2);
        auto mag = abs(value);

        if(mag > max)
            max = mag;

        average += mag / (double(maxSize)/2);
    }

    InputEvent e;
    e.mType = InputEvent::EType::TypeVoice;
    e.mVoice = max;
    mListener->OnInputEvent(e);

    //qDebug() << maxSize << "Voice: " << timer.restart() << "ms";

    return maxSize;
}

DWORD CALLBACK InputEventManager::WasapiProc(void *buffer, DWORD length, void *user)
{
    Q_UNUSED(buffer)
    Q_UNUSED(length)
    Q_UNUSED(user)
    return 1;
}

void InputEventManager::FftHandler()
{
    const int FFT_BUFFER_SIZE = 2048;
    const int FFT_REAL_SIZE = FFT_BUFFER_SIZE / 8;
    const int NUM_BINS = 64;
    const int DIVIDER = FFT_REAL_SIZE / NUM_BINS;

    float buffer[FFT_BUFFER_SIZE];
    BASS_WASAPI_GetData(buffer, BASS_DATA_FFT4096);

    InputEvent e;
    e.mType = InputEvent::EType::TypeSound;
    e.mSound = QVector<qreal>(NUM_BINS, 0.0);
    //    memcpy(&e.mSound[0], buffer, sizeof(buffer));
    for(int i = 0; i < FFT_REAL_SIZE; i++)
        e.mSound[i/DIVIDER] += qreal(buffer[i] / DIVIDER);

    for(int i = 0; i < NUM_BINS; i++)
        e.mSound[i] += sqrt(e.mSound[i]);

    mListener->OnInputEvent(e);
}

BOOL InputEventManager::EnumWindowsProc(HWND hwnd, LPARAM lParam)
{
    int len = GetWindowTextLength(hwnd) + 1;
    std::vector<wchar_t> buf(len);
    GetWindowText(hwnd, &buf[0], len);
    std::wstring wide = &buf[0];
    std::string title(wide.begin(), wide.end());
    if(title.empty())
        return true;

    if(title.find(" - YouTube") != std::string::npos) {
        qDebug() << "Music window: " << title.c_str();
        mMusicWindowHwnd = hwnd;
        return false;
    }

    return true;
}

void InputEventManager::MusicTitleHandler()
{
    if(mMusicWindowHwnd == NULL) {
        InputEvent e;
        e.mType = InputEvent::EType::TypeMusicTitle;
        e.mMusicTitle = QString("Open browser with YouTube card");

        mListener->OnInputEvent(e);
        EnumWindows(EnumWindowsProc, NULL);
        return;
    }
    auto len = GetWindowTextLength(mMusicWindowHwnd) + 1;
    std::vector<wchar_t> buf(len);
    GetWindowText(mMusicWindowHwnd, &buf[0], len);
    std::wstring wide = &buf[0];
    std::string title(wide.begin(), wide.end());
    if(title.empty()) {
        EnumWindows(EnumWindowsProc, NULL);
        return;
    }

    auto endIndex = title.find(" - YouTube");

    if(endIndex == std::string::npos) {
        InputEvent e;
        e.mType = InputEvent::EType::TypeMusicTitle;
        e.mMusicTitle = QString("Open browser with YouTube card");

        mListener->OnInputEvent(e);
        EnumWindows(EnumWindowsProc, NULL);
        return;
    }

    auto musicTitle = title.substr(0, endIndex);
    InputEvent e;
    e.mType = InputEvent::EType::TypeMusicTitle;
    e.mMusicTitle = QString(musicTitle.c_str());

    mListener->OnInputEvent(e);
}
