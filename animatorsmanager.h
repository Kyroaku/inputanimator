#ifndef ANIMATORSMANAGER_H
#define ANIMATORSMANAGER_H

#include "animator.h"
#include "inputeventlistener.h"

#include <QVector>
#include <QSharedPointer>


class AnimatorsManager : public QObject, public InputEventListener
{
    Q_OBJECT

private:
    QVector<QSharedPointer<Animator>> mAnimators;

public:
    AnimatorsManager();
    ~AnimatorsManager() override;

    void RegisterAnimator(QSharedPointer<Animator> animator);

    void OnInputEvent(InputEvent event) override;

    Q_INVOKABLE void registerAnimators(QQuickItem *item);

    Q_INVOKABLE void unregisterAnimators();
};

#endif // ANIMATORSMANAGER_H
