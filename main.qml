import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.4
import Qt.labs.folderlistmodel 2.12

Window {
    id: window
    visible: true
    title: qsTr("Input Animator | ESC to back")
    width: 800
    height: 600
    //    flags: /*Qt.WindowTransparentForInput */ | Qt.FramelessWindowHint | Qt.Window |  Qt.WindowStaysOnTopHint
    //    color: 'transparent'
    flags: Qt.Window | Qt.MSWindowsFixedSizeDialogHint | Qt.WindowTitleHint | Qt.WindowCloseButtonHint
    color: '#00ff00'

    Item {
        focus: true

        Keys.onPressed: {
            if (event.key === Qt.Key_Escape) {
                AnimatorsManager.unregisterAnimators();
                animator.source = ""
                animatorPicker.enabled = true
                animatorPicker.visible = true
                animator.enabled = false
                animator.visible = false
                window.width = 800
                window.height = 600
            }
        }
    }

    Loader {
        id: animator
        enabled: false
        visible: false
        layer.enabled: false

        onSourceChanged: {
            if(source.toString().length > 0) {
                animatorPicker.enabled = false
                animatorPicker.visible = false
                animator.enabled = true
                animator.visible = true
                window.width = animator.width
                window.height = animator.height
                AnimatorsManager.registerAnimators(animator.item)
            }
        }

        onLoaded: {
            console.debug("Picked animator loaded sucessfully.")
        }

        MouseArea {
            anchors.fill: parent

            property variant clickPos: "1,1"

            onPressed: {
                clickPos  = Qt.point(mouse.x,mouse.y)
            }

            onPositionChanged: {
                var delta = Qt.point(mouse.x-clickPos.x, mouse.y-clickPos.y)
                window.x += delta.x;
                window.y += delta.y;
            }
        }
    }

    Item {
        id: animatorPicker
        anchors.fill: parent
        enabled: true
        visible: true

        Image {
            id: image
            anchors.fill: parent
            source: "background-4232859_1280.png"
            fillMode: Image.PreserveAspectCrop
        }

        Loader {
            id: animatorsLoader
            source: "https://gitlab.com/Kyroaku/inputanimator/-/raw/release_v3/Animators/AnimatorsLoader.qml"

            FolderListModel {
                id: fileModel
                showDirs: false
                nameFilters: ["*.qml"]
                folder: "file:Animators"

                onCountChanged: {
                    var animators = repeater.model
                    for(var i = 0; i < fileModel.count; i++) {
                        animators.push("file:" + fileModel.get(i, "filePath"))
                    }
                    repeater.model = animators
                }
            }

            onLoaded: {
                console.debug("Animators list source: " + source)
                console.debug("Animators list loaded: " + item.animators.length + " animators")
                var animators = repeater.model
                for(var i = 0; i < item.animators.length; i++) {
                    animators.push(item.animators[i])
                }
                repeater.model = animators
            }

            onStatusChanged: {
                switch(status) {
                case Loader.Error:
                    console.error("Animators list loading error!")
                    break

                case Loader.Loading:
                    console.debug("Loading animators list...")
                    break
                }
            }
        }

        ScrollView {
            id: scrollView
            wheelEnabled: true
            anchors.fill: parent
            clip: true
            Grid {
                id: grid
                columns: 3
                anchors.fill: parent
                horizontalItemAlignment: Grid.AlignHCenter
                verticalItemAlignment: Grid.AlignVCenter

                Repeater {
                    id: repeater
                    model: []

                    delegate: Item {
                        id: element
                        width: scrollView.width/3
                        height: scrollView.width/3
                        clip: true

                        Loader {
                            id: animatorItem
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.horizontalCenter: parent.horizontalCenter
                            source: modelData
                        }
                        Text {
                            color: "#440000"
                            text: {
                                if(modelData.split(":")[0] === "file")
                                    "(local)\n" + modelData.split("/").pop()
                                else
                                    "(remote)\n" + modelData.split("/").pop()
                            }
                            anchors.topMargin: 10
                            anchors.fill: parent
                            verticalAlignment: Text.AlignTop
                            horizontalAlignment: Text.AlignHCenter
                            font.weight: Font.ExtraBold
                            font.bold: true
                            font.pointSize: 16
                            font.family: "Arial"
                        }

                        Rectangle {
                            anchors.fill: parent
                            color: 'transparent'
                            border.width: 3
                            border.color: "#440000"
                            radius: 10
                        }
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                console.debug("Picked animator: " + animatorItem.source)
                                animator.source = animatorItem.source
                            }
                        }
                    }
                }
            }
        }
    }
}

/*##^##
Designer {
    D{i:2;anchors_height:400;anchors_width:400;anchors_x:63;anchors_y:63}
}
##^##*/
