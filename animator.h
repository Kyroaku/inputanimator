#ifndef IANIMATOR_H
#define IANIMATOR_H

#include "inputevent.h"

#include <QQuickItem>
#include <QSharedPointer>


class Animator : public QObject
{
    Q_OBJECT
    Q_PROPERTY(InputEvent::EType eventType READ eventType WRITE setEventType)
    Q_PROPERTY(InputEvent::EKeyState eventKeyState READ eventKeyState WRITE setEventKeyState)
    Q_PROPERTY(int eventKey READ eventKey WRITE setEventKey)
    Q_PROPERTY(InputEvent::EMouseButton eventMouseButton READ eventMouseButton WRITE setEventMouseButton)
    Q_PROPERTY(QPoint eventMouse READ eventMouse WRITE setEventMouse)

public:
    InputEvent mEvent;

public:
    Animator();
    virtual ~Animator();

    virtual void OnActivate(InputEvent event);

    InputEvent::EType eventType();
    InputEvent::EKeyState eventKeyState();
    InputEvent::KeyType eventKey();
    InputEvent::EMouseButton eventMouseButton();
    QPoint eventMouse();

    void setEventType(InputEvent::EType event);
    void setEventKeyState(InputEvent::EKeyState state);
    void setEventKey(InputEvent::KeyType key);
    void setEventMouseButton(InputEvent::EMouseButton button);
    void setEventMouse(QPoint mouse);

signals:
    void activate(
            InputEvent::EType inputType,
            InputEvent::EKeyState inputKeyState,
            int inputKey,
            InputEvent::EMouseButton inputButton,
            QPointF inputMouse,
            int inputVoice,
            QVector<qreal> inputSound,
            QString inputMusicTitle
            );
};

#endif // IANIMATOR_H
