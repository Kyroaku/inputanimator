#include "animatorsmanager.h"
#include "inputeventmanager.h"

#include <qdebug.h>

AnimatorsManager::AnimatorsManager()
{

}

AnimatorsManager::~AnimatorsManager()
{
    InputEventManager::Deinit();
}

void AnimatorsManager::RegisterAnimator(QSharedPointer<Animator> animator)
{
    mAnimators.append(animator);
}

void AnimatorsManager::registerAnimators(QQuickItem *item)
{
    if(item == nullptr)
        return;

    auto inputAnimatorList = item->findChildren<Animator*>("InputAnimator");
    for(auto &inputAnimator : inputAnimatorList) {
        RegisterAnimator(QSharedPointer<Animator>(inputAnimator));
        InputEventManager::Init(inputAnimator->eventType());
    }
}

void AnimatorsManager::unregisterAnimators()
{
    mAnimators.clear();
    InputEventManager::Deinit();
}

void AnimatorsManager::OnInputEvent(InputEvent event)
{
    for(auto &animator : mAnimators)
    {
        if(animator->mEvent.mType != event.mType)
            continue;

        if(event.mType == InputEvent::EType::TypeKeyboardKey)
        {
            if(
                    (animator->mEvent.mKeyState == event.mKeyState ||
                     animator->mEvent.mKeyState == InputEvent::EKeyState::StateAny
                     ) &&
                    (animator->mEvent.mKey == event.mKey ||
                     animator->mEvent.mKey == InputEvent::EKey::KeyAny)
                    )
            {
                animator->OnActivate(event);
            }
        }
        else if(event.mType == InputEvent::EType::TypeMouseButton)
        {
            if(
                    (animator->mEvent.mKeyState == event.mKeyState ||
                     animator->mEvent.mKeyState == InputEvent::EKeyState::StateAny
                     ) &&
                    (animator->mEvent.mMouseButton == event.mMouseButton ||
                     animator->mEvent.mMouseButton == InputEvent::EMouseButton::ButtonAny)
                    )
            {
                animator->OnActivate(event);
            }
        }
        else
        {
            animator->OnActivate(event);
        }
    }
}
